import * as types from './types';
import * as Functions from './functions'

export function contextReducer(state, action) {
    switch (action.type) {
        case types.AUTHENTICATE: {
            return Functions.authenticate(state, action);  
        }
        case types.REGISTER: {
            return Functions.register(state, action);
        }
        case types.LOGOUT: {
            return Functions.logout(state, action);
        }
        case types.SET_CURRENT_GAME: {
            return Functions.set_current_game(state, action);
        }
        case types.REMOVE_CURRENT_GAME: {
            return Functions.remove_current_game(state, action);
        }
        default: {
            throw new Error(`Unhandled action type: ${action.type}`);
        }
    }
}