export default [
    {
        id: 0,
        text: "La/Le plus radin(e)",
        swallow : "boit {nbSwallow}",
        sex : "all",
        isRule: false
    },
    {
        id: 1,
        text: "{player} , bois 1 gorgée et fait 3 tours sur toi-même.",
        swallow : "Fini ton verre si tu tombes.",
        sex : "all",
        isRule: false
    },
    {
        id: 2,
        text: "{player} fait un \"Tu ris, tu perds\" avec la personne de ton choix.",
        swallow : "Le perdant boit {nbSwallow}.",
        sex: "all",
        isRule: false
    },
];
