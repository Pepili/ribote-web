import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import { optionFilter, optionTri } from '../../Assets/game';
import colorStyle from './style'

function Filter({ selects, setSelects, multiSelects, setMultiSelects }) {
  const animatedComponents = makeAnimated();

  const handleChange = e => {
    setMultiSelects(Array.isArray(e) ? e.map(x => x.value) : []);
  };
  return (
    <div className="filter">
      <ul>
        <li>
          <Select
            value={optionFilter.filter(x => multiSelects.includes(x.value))}
            onChange={handleChange}
            closeMenuOnSelect={false}
            components={animatedComponents}
            isMulti
            styles={colorStyle}
            options={optionFilter}
            width="300px"
            placeholder="Filtres"
          />
        </li>
        <li>
          <Select
            value={optionTri.find(x => x.value === selects)}
            onChange={obj => {
              setSelects(obj.value);
            }}
            options={optionTri}
            styles={colorStyle}
            width="100px"
            placeholder="Trier"
          />
        </li>
      </ul>
    </div>
  );
}

export default Filter;
