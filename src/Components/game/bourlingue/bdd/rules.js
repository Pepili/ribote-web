export default [
    {
        id: 0,
        text: "On ne peut parler qu'avec les yeux fermés !",
        swallow : "buvez {nbSwallow} à chaques manques",
        sex : "all",
        isRule: true
    },
    {
        id: 1,
        text: "{player} à l'oeil de serpent ! Dès qu'un joueur te regarde dans les yeux,",
        swallow : "il/elle boit. Si personne ne t'as regardé tu bois.",
        sex : "all",
        isRule: true
    },
    {
        id: 2,
        text: "{player}, tu dois crier \"Wazaaa!!\" avec la langue sorti à chaque fois que tu bois jusqu'à ce que le jeu te dise d'arrêter.",
        swallow : "En cas d'oublie bois {nbSwallow}",
        sex : "all",
        isRule: true
    },
    {
        id: 3,
        text: "{player}, à chaque fois que tu poseras ta main sur la table, tous les autres joueurs devront le faire.",
        swallow : "Le dernier joueur à poser sa main boit  {nbSwallow}",
        sex : "all",
        isRule: true
    },
    {
        id: 4,
        text: "{player}, imite la poule à chaque fois que tu parles.",
        swallow : "En cas d'oublie bois {nbSwallow}",
        sex : "all",
        isRule: true
    },
]