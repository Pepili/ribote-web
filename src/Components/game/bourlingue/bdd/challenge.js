export default [
    {
        id: 0,
        text: "{player}, pendant {nbRound}, tu devras attendre 2secondes entre chaque mot.",
        swallow : "{nbSwallow} à chaque manque",
        sex : "all",
        isRule: false
    },
    {
        id: 1,
        text: "{player}, donne l'âge de toutes les personnes qui participent.",
        swallow : "1 gorgée par erreur.",
        sex : "all",
        isRule: false
    },
];
