import React from 'react';
import { contextReducer } from './reducer';

export const Context = React.createContext();
export function ContextProvider({ children }) {
  const [state, dispatch] = React.useReducer(contextReducer, {
    id: null,
    username: null,
    isAuthenticated: false,
    currentGame: null
  });
  const value = { state, dispatch };
  return <Context.Provider value={value}>{children}</Context.Provider>;
}