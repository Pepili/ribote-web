import challenge from  './challenge';
import distribute from  './distribute';
import dryAss from  './dryAss';
import duel from  './duel';
import game from  './game';
import global from  './global';
import rules from  './rules';
import solo from  './solo';

export default function getData(name) {
    switch (name) {
        case 'challenge':
            return challenge;
        case 'distribute':
            return distribute;
        case 'dryAss':
            return dryAss;
        case 'duel':
            return duel;
        case 'game':
            return game;
        case 'global':
            return global;
        case 'rules':
            return rules;
        case 'solo':
            return solo;
    }
};