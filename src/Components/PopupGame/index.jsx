import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faBook,
  faMars,
  faVenus,
  faMarsAndVenus,
  faXmark,
} from '@fortawesome/free-solid-svg-icons';
import Select from 'react-select';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { colorStyles, getPlayerData } from './popupOption';
import { isUsernameValid } from '../utils/validator';

function PopupGame({ game, setPopupGame }) {
  const [numberSelect, setNumberSelect] = useState();
  const [pseudoPlayer, setNouveauPseudoPlayer] = useState('');
  const nbPlayers = game.players;
  const numberPlayers = getPlayerData(nbPlayers);
  let inputPseudoList = [];
  if (numberSelect !== undefined) {
    for (let i = 0; i < numberSelect; i++) {
      inputPseudoList.push(
        <div key={i}>
          <input
            type="text"
            required
            className={`inputPseudo ${isUsernameValid.test(pseudoPlayer) ? '' : 'errorPseudo'}`}
          />
          <FontAwesomeIcon icon={faMars} />
          <FontAwesomeIcon icon={faVenus} />
          <FontAwesomeIcon icon={faMarsAndVenus} />
        </div>,
      );
    }
  }

  const nameLink = game.name.normalize('NFD').replace(/[\u0300-\u036f,:]/g, '').replace(/\s/g, '').toLowerCase();
  const handleClick = () => {
    setPopupGame(false);
  };
  return (
    <div className="popup">
      <img />
      <div className="popupHeader">
        <FontAwesomeIcon icon={faBook} className="iconRules" />
        <FontAwesomeIcon icon={faXmark} className="iconCross" onClick={handleClick} />
      </div>
      <h4>{game.name}</h4>
      <div className="popupElement">
        <p className="popupDescription">{game.description}</p>
        <h5>Parametres</h5>
        <div className="popupForm">
          <Select
            value={numberPlayers.find(x => x.value === numberSelect)}
            onChange={obj => {
              setNumberSelect(obj.value);
            }}
            options={numberPlayers}
            styles={colorStyles}
            placeholder="Nombre de joueurs"
          />
          <div className="popupInput">{inputPseudoList}</div>
          <Link
            className="popupButton"
            to={'/game/' + nameLink}
            state={{ gameData: game }}
          >
            Commencer
          </Link>
        </div>
      </div>
    </div>
  );
}

export default PopupGame;
