import { games } from '../Assets/game.js';
import { useState, useEffect } from 'react';
import PopupGame from './PopupGame';

function GameList({ selects, multiSelects }) {
  const [order, setOrder] = useState(games);
  const [nameGame, setNameGame] = useState('');
  const [popupGame, setPopupGame] = useState(false);

  useEffect(() => {
    let orderGame;
    if (selects === 'A-Z') {
      orderGame = [...order].sort((a, b) => {
        if (a.name > b.name) return 1;
        if (a.name < b.name) return -1;
        return 0;
      });
      setOrder(orderGame);
    } else if (selects === 'Z-A') {
      orderGame = [...order].sort((a, b) => {
        if (a.name < b.name) return 1;
        if (a.name > b.name) return -1;
        return 0;
      });
      setOrder(orderGame);
    }
  }, [selects]);

  useEffect(() => {
    const object = multiSelects.reduce((obj, item) => ((obj[item.key] = item.value), obj), {});
    const orderGame = [...games].filter(e => {
      for (let key in object) {
        if (e[key] === undefined || e[key] !== object[key]) return false;
      }
      return true;
    });
    setOrder(orderGame);
  }, [multiSelects]);

  const handleClick = game => {
    setNameGame(game);
    setPopupGame(true);
  };

  return (
    <div className="gameList">
      {order.map(game => (
        <div className="game" key={game.name} onClick={() => handleClick(game)}>
          <div className="headerGame">
            <h3>{game.name}</h3>
            <div className="headerGameInfo">
              <p>{game.alcool ? 'Alcool' : 'Sans alcool'}</p>
              <p>{game.players > 2 ? '2-8j' : '2j'}</p>
            </div>
          </div>
          <p className="descriptionGame">{game.description}</p>
        </div>
      ))}
      {popupGame ? <PopupGame game={nameGame} setPopupGame={setPopupGame} /> : null}
    </div>
  );
}

export default GameList;
