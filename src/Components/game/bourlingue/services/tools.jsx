const randomPlayers = (players, filterPlayer = null) => {
    let player = players[Math.floor(Math.random() * (players.length))];
    if (filterPlayer && filterPlayer.name === player.name) {
        return randomPlayers(players, filterPlayer);
    }
    return player;
}

export const randomRound = (currentRound, totalRound) => {
    let round = 1+((Math.floor(Math.random()*5)));
    const leftrounds = totalRound - currentRound
    if (round > leftrounds) {
        round = leftrounds - 1;
    }
    return round;
}

const randomSwallow = () => {
    return 1+((Math.floor(Math.random()*5)));
}

export function formatParameters(parameters) {
    for (const key in parameters) {
        if (typeof parameters[key] === 'string') {
            parameters[key] = Number(parameters[key]);
        } else {
            for (const key2 in parameters[key]) {
                parameters[key][key2] = Number(parameters[key][key2]);
            }
        }
    }

    return parameters;
} 

export function formatText(game, players, currentRound, totalRound, swallow = 0) {
    const player1 = randomPlayers(players);
    const player2 = randomPlayers(players, player1);
    const rd = randomRound(currentRound, totalRound);
    const rs = randomSwallow();
    const regex = /{nbSwallow}/gi;
    let text = `${game.text} ${game?.swallow || ''}`
    const hasSwallow = regex.exec(text);
    text = text
        .replaceAll(`{player}`, player1.name)
        .replaceAll(`{player1}`, player1.name)
        .replaceAll(`{player2}`, player2.name)
        .replaceAll(`{nbRound}`, (rd > 1 ? `${rd} tours` : `${rd} tour`))
        .replaceAll(`{nbSwallow}`, (rd > 1 ? `${rd} gorgées` : `${rd} gorgée`));

        if (hasSwallow) {
        swallow =  swallow + rs
    }
    return {swallow, text};
}