import { useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { Context } from '../context';
import { AUTHENTICATE } from '../context/reducer/types';

export function useAuthentification() {
    const { 
        state: { isAuthenticated },
        dispatch
    } = useContext(Context);
    const navigate = useNavigate();
    
    let userFromStorage = sessionStorage.getItem('user');
    if (userFromStorage) {
        userFromStorage = JSON.parse(userFromStorage)
    }

    useEffect(() => {
        if (!isAuthenticated) {
            if (userFromStorage?.id && userFromStorage?.isAuthenticated) {
                dispatch({ type: AUTHENTICATE, data: userFromStorage })
            } else {
                // case of user is no authenticated/register whatever redirect to /
                navigate('/login');
            }
        }
    }, [isAuthenticated])

  return isAuthenticated;
}