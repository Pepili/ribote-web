import chroma from 'chroma-js';

export const colorStyles = {
  control: styles => ({
    ...styles,
    backgroundColor: '#01334c',
    color: 'white',
    textAlign: 'center',
    fontSize: '14px',
    width: '210px',
    cursor: 'pointer',
  }),
  option: (styles, state) => {
    const color = chroma(state.data.color);
    return {
      ...styles,
      backgroundColor: state.isSelected
        ? state.data.color
        : state.isFocused
        ? color.alpha(0.2).css()
        : '#01334c',
      color: state.isDisabled
        ? '#ccc'
        : state.isSelected
        ? chroma.contrast(color, 'white') > 2
          ? 'white'
          : 'black'
        : state.data.color,
      cursor: 'pointer',
      textAlign: 'center',
      borderLeft: '1px solid white',
      borderRight: '1px solid white',
    };
  },

  singleValue: styles => ({
    ...styles,
    color: 'white',
  }),
};

export const getPlayerData = nbPlayers => {
  const numberPlayers = [];
  const colorPlayers = [
    '#e77d1d',
    '#fed430',
    '#ae3e4c',
    '#00B8D9',
    '#3FBF3F',
    '#BF7F3F',
    '#BF3FBF',
  ];
  for (let i = 2; i < nbPlayers; i++) {
    numberPlayers.push({ value: i, label: i + ' joueurs', color: colorPlayers[i - 2] });
  }
  return numberPlayers;
};
