import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCocktail } from '@fortawesome/free-solid-svg-icons';

import { isUsernameValid } from '../utils/validator';
import useHome from './useHome';

export default function Home() {
  const { username, handleUsername, handleSubmit } = useHome();

  return (
    <div className="home">
      <h1>
        <FontAwesomeIcon className="home-cocktail" icon={faCocktail} />
        Ribote
        <FontAwesomeIcon className="home-cocktail" icon={faCocktail} />
      </h1>
      <p>L'application incontournable pour les jeux de soirées arrosés ou non !</p>
      <div className="inputGroup">
        <label className="labelPseudo">
          <input
            id="name"
            className={`inputPseudo ${isUsernameValid(username) ? '' : 'errorPseudo'}`}
            type="text"
            required
            value={username}
            onChange={handleUsername}
          />
          <span className="labelHolder">Indique ton username</span>
        </label>
      </div>
      <button
        disabled={!isUsernameValid(username)}
        className="home-button"
        style={isUsernameValid(username) ? null : { pointerEvents: 'none' }}
        onClick={handleSubmit}
      >
        Commencer
      </button>
      <div className="credits">Créé par Lucas Roberts, Lisa Genest et Mathilde Fournel</div>
    </div>
  );
}
