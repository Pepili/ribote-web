export const typeRateAppearance = [
    {0 : 20},
    {1 : 20},
    {2 : 20},
    {3 : 10},
    {4 : 10},
    {5 : 10},
    {6 : 5},
    {7 : 3},
    {8 : 2},
];

export const typeID = {
    0 : 'global',
    1 : 'solo',
    2 : 'distribute',
    3 : 'challenge',
    4 : 'duel',
    5 : 'game',
    6 : 'rules',
    7 : 'endRules',
    8 : 'dryAss'
};
