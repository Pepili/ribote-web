export const AUTHENTICATE = 'authenticate';
export const REGISTER = 'register';
export const LOGOUT = 'logout';
export const SET_CURRENT_GAME = 'set_current_game';
export const REMOVE_CURRENT_GAME = 'remove_current_game';