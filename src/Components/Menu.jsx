import { useState } from 'react';
import Banner from './Banner';
import Filter from './Filter';
import GameList from './GameList';

function Menu() {
  const [selects, setSelects] = useState('');
  const [multiSelects, setMultiSelects] = useState([]);

  return (
    <div className="menu">
      <Banner />
      <h2>Choisis ton jeu !</h2>
      <Filter
        selects={selects}
        setSelects={setSelects}
        multiSelects={multiSelects}
        setMultiSelects={setMultiSelects}
      />
      <GameList selects={selects} multiSelects={multiSelects} />
    </div>
  );
}

export default Menu;
