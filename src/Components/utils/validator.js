import { USERNAME_REGEX } from '../../utils';

export function isUsernameValid(username) {
  return USERNAME_REGEX.test(username);
}
