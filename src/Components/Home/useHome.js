import { useState, useContext, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import { isUsernameValid } from '../utils/validator';
import { Context } from '../utils/context';
import { REGISTER } from '../utils/context/reducer/types';

export default function useHome() {
  const {
    state: { isAuthenticated },
    dispatch,
  } = useContext(Context);

  const [username, setUsername] = useState('');

  const handleUsername = event => setUsername(event.currentTarget.value);

  const handleSubmit = event =>
    isUsernameValid(username)
      ? dispatch({ type: REGISTER, data: username })
      : event.preventDefault();

  const navigate = useNavigate();
  useEffect(() => {
    // case of user is authenticated/register whatever redirect to /menu
    if (isAuthenticated) {
      navigate('/');
    }
  }, [isAuthenticated, navigate]);

  return {
    username,
    handleUsername,
    handleSubmit,
  };
}
