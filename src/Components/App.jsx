import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Home from './Home';
import Menu from './Menu';
import Tafeoupa from './game/Tafeoupa';
import Bourlingue from './game/bourlingue';
import { useAuthentification } from './utils/customHooks/auth.hook';

function App() {
  useAuthentification();

  return (
    <div>
      <Routes>
        <Route path="/login" element={<Home />} />
        <Route path="/" element={<Menu />} />
        <Route path="/game/tafeoupa" element={<Tafeoupa />} />
        <Route path="/game/labourlingue" element={<Bourlingue />} />
      </Routes>
    </div>
  );
}

export default App;
