export default [
    {
        id: 0,
        text: "Tous ceux ou celles qui portent des chaussures",
        swallow : "boient {nbSwallow}",
        sex : "all",
        isRule: false
    },
    {
        id: 1,
        text: "Tous ceux ou celles qui ont déja pris l'avion",
        swallow : "boient {nbSwallow}",
        sex : "all",
        isRule: false
    },
    {
        id: 2,
        text: "Tous ceux ou celles qui ont les yeux bleus",
        swallow : "boient {nbSwallow}",
        sex : "all",
        isRule: false
    },
    {
        id: 3,
        text: "Celles et ceux qui ont déjà habité dans plus de 2 villes",
        swallow : "boient {nbSwallow}",
        sex : "all",
        isRule: false
    },
    {
        id: 4,
        text: "Tous ceux ou celles qui ont un iPhone",
        swallow : "boient {nbSwallow}",
        sex : "all",
        isRule: false
    },
    {
        id: 5,
        text: "Celles et ceux qui ont la lettre A dans leur prénom",
        swallow : "boient {nbSwallow}",
        sex : "all",
        isRule: false
    },
    {
        id: 6,
        text: "Celles et ceux qui étaient déjà saouls hier",
        swallow : "boient {nbSwallow}",
        sex : "all",
        isRule: false
    },
    {
        id: 7,
        text: "Ceux/Celles qui ont un paquet de clopes",
        swallow : "boivent leur nombre de cigarettes restantes en gorgées. Si il n'y en à qu'une, buvez {nbSwallow} dans un nouveau verre",
        sex : "all",
        isRule: false
    },   
    {
        id: 8,
        text: "Tourné des verres ! Passe ton verre à gauche. Les joueurs ne jouant pas avec de l'alcool ne participe pas.",
        swallow : "",
        sex : "all",
        isRule: false
    },
    {
        id: 9,
        text: "Si ton prénom commence par une voyelle,",
        swallow : "boit {nbSwallow}.",
        sex : "all",
        isRule: false
    }, 
    {
        id: 10,
        text: "Si tu as déjà dansé nu chez toi avec la musique à fond,",
        swallow : "boit {nbSwallow}.",
        sex : "all",
        isRule: false
    }, 
    {
        id: 11,
        text: "Ceux qui ont déjà regardé un hentai et qui ont aimé",
        swallow : "boient {nbSwallow}.",
        sex : "all",
        isRule: false
    }, 
    {
        id: 12,
        text: "Si tu aimes les choux de bruxelles,",
        swallow : "bois {nbSwallow}.",
        sex : "all",
        isRule: false
    }, 
    {
        id: 13,
        text: "Ceux qui ont un \"E\" dans leurs nom",
        swallow : "boient {nbSwallow}.",
        sex : "all",
        isRule: false
    }, 
    {
        id: 14,
        text: "Tout les joueurs qui ne boient pas d'alcool doivent boire une gorgée avec le nez (en utilisant une paille)",
        swallow : "",
        sex : "all",
        isRule: false
    }, 
    {
        id: 15,
        text: "Tout les joueurs qui ne boient pas d'alcool doivent voter pour la personne qui, selon eux, est la plus perverse.",
        swallow : "Cette personne devras boire {nbSwallow}.",
        sex : "all",
        isRule: false
    }, 
];
