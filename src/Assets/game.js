export const optionFilter = [
  { value: { key: 'alcool', value: true }, label: 'alcool', color: '#e77d1d' },
  {
    value: { key: 'Alcool', value: false },
    label: 'sans alcool',
    color: '#fed430',
  },
  { value: { key: 'Players', value: 2 }, label: '2j', color: '#ae3e4c' },
  { value: { key: 'players', value: 8 }, label: '2-8j', color: '#00B8D9' },
];

export const optionTri = [
  { value: 'A-Z', label: 'A-Z', color: '#00B8D9' },
  { value: 'Z-A', label: 'Z-A', color: '#00B8D9' },
];

export const games = [
  {
    name: 'Taféoupa',
    imageUrl: '',
    alcool: true,
    multitel: false,
    players: 8,
    description:
      'Découvre les petits secrets de tes amis (et dévoile les tiens) en répondant si oui ou non tu as déjà fait ce que le jeu te demande',
  },
  {
    name: 'Le bananier',
    imageUrl: '',
    alcool: true,
    multitel: false,
    players: 8,
    description:
      'Découvre les règles exclusives du bananier et défis tes amis (avec un jeu de carte à ta disposition)',
  },
  {
    name: 'Karnage',
    imageUrl: '',
    Alcool: false,
    multitel: false,
    players: 8,
    description:
      "Il est temps de mettre un peu le bazar dans ton groupe d'ami avec les questions indiscretes de Karnage!",
  },
  {
    name: 'La bourlingue',
    pathName: 'bourlingue',
    imageUrl: '',
    alcool: true,
    multitel: false,
    players: 8,
    color: 'pink',
    description:
      "Attention à vos foies avec la bourlingue! Des questions et des défis en tout genre qui illustreront parfaitement le nom ribote de l'application!",
    parameters: {
      duration: '30',
      round: '6',
      swallow: '6',
      rateAppearance: {
          0: '20',
          1: '20',
          2: '20',
          3: '10',
          4: '10',
          5: '10',
          6: '5',
          7: '3',
          8: '2',
      }
    }
  },
  {
    name: "Canas'Saoul",
    imageUrl: '',
    alcool: true,
    multitel: false,
    players: 8,
    description:
      "Tu aimes parier? Cette fois-ci pas d'argent mais des gorgées en jeux sur cette course hippique (choisis bien ton cheval)",
  },
];
