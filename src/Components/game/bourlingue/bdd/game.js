export default [
    {
        id: 0,
        text: "Plutôt gros seins ou grosse fesses ? La minorité",
        swallow : "boient {nbSwallow}",
        sex : "all",
        isRule: false
    },
    {
        id: 1,
        text: "Chacun votre tour, donnez un nom d'acteur chauve. {player} commence.",
        swallow : "Le premier qui ne trouve pas ou répète boit {nbSwallow}.",
        sex : "all",
        isRule: false
    },
    {
        id: 2,
        text: "Plutôt Kinder choco bons ou Kinder bueno. La minorité.",
        swallow : "boit {nbSwallow}.",
        sex : "all",
        isRule: false
    },
    {
        id: 3,
        text: "Plutôt Samsung ou Apple. La minorité",
        swallow : "boient {nbSwallow}.",
        sex : "all",
        isRule: false
    }
];
