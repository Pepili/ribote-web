import chroma from 'chroma-js';

export default {
    control: (styles, { selectProps: { width } }) => ({
      ...styles,
      backgroundColor: '#01334c',
      color: 'white',
      width: width,
      cursor: 'pointer',
    }),
    option: (styles, state) => {
      const color = chroma(state.data.color);
      return {
        ...styles,
        backgroundColor: state.isSelected
          ? state.data.color
          : state.isFocused
          ? color.alpha(0.2).css()
          : '#01334c',
        color: state.isDisabled
          ? '#ccc'
          : state.isSelected
          ? chroma.contrast(color, 'white') > 2
            ? 'white'
            : 'black'
          : state.data.color,
        cursor: 'pointer',
        textAlign: 'center',
        borderLeft: '1px solid white',
        borderRight: '1px solid white',
      };
    },

    singleValue: styles => ({
      ...styles,
      color: 'white',
    }),
    multiValue: (styles, state) => {
      const color = chroma(state.data.color);
      return {
        ...styles,
        backgroundColor: color.alpha(0.1).css(),
      };
    },
    multiValueLabel: (styles, state) => ({
      ...styles,
      color: state.data.color,
    }),
    multiValueRemove: (styles, state) => ({
      ...styles,
      color: state.data.color,
      ':hover': {
        backgroundColor: state.data.color,
        color: '#01334c',
      },
    }),
  };