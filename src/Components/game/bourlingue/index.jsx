import { useContext, useEffect, useState } from "react";
import { useLocation } from 'react-router-dom'
import Banner from "../../Banner";
import { typeID } from "./services/constant";
import { Context } from "../../utils/context";
import * as gameUtils from './services/gameUtils';

const players = [{name: 'Lucas'}, {name: 'Lisa'}, {name: 'Mathilde'}]
const parameters = {
  duration: '30',
  round: '6',
  swallow: '6',
  rateAppearance: {
      0: '20',
      1: '20',
      2: '20',
      3: '10',
      4: '10',
      5: '10',
      6: '5',
      7: '3',
      8: '2',
  }
}

function Bourlingue() {
    const {state: {gameData}} = useLocation()
    const {dispatch} = useContext(Context);
    const [gameCore, setGameCore] = useState(null);
    const [game, setGame] = useState(null);
    const [round, setRound] = useState(null);
    const [nbSwallow, setNbSwallow] = useState(0);
    const [currentText, setCurrentText] = useState('');
    const [currentRules, setCurrentRules] = useState([]);

    useEffect(() => gameUtils.initGame(dispatch, players, gameData?.parameters || parameters, gameCore, game, setGameCore, setGame, setRound, setNbSwallow, setCurrentText));
    useEffect(() => gameUtils.getText(gameCore, game, round, nbSwallow, currentRules, setCurrentRules, setNbSwallow, setCurrentText), [round]);
    useEffect(() => gameUtils.saveGame(dispatch, gameCore, game, round, nbSwallow, currentText), [round, currentText, nbSwallow]);

    return (
        <div>
            <Banner />
            <h1>La Bourlingue</h1>

            <div onClick={() => { if (round < (game?.length)) {setRound(round + 1)} }}>
                {round !== null && 
                    <>
                    {(!game[round] || round === (game?.length)) &&
                        <p>Fin du game, {nbSwallow} gorgées ont été bu durant cette partie</p>
                    }
                    {(game[round] && round < (game?.length)) &&
                        <>
                            <p>{typeID[game[round].gameType]}</p>
                            <p>{currentText}</p>
                            <div>
                                {currentRules.map((rule, i) => (
                                    <p key={i}>{rule.text} ({rule.endRound} tours)</p>
                                ))}
                            </div>
                        </>
                    }
                    </>
                }
            </div>
        </div>
    );
}

export default Bourlingue;
