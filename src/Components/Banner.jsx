import { useContext } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCocktail } from '@fortawesome/free-solid-svg-icons';
import { Context } from './utils/context';

function Banner() {
  const { state: { username } } = useContext(Context);

  return (
    <div className="banner">
      <div className="pseudoUser">
        <p>Pseudo: <span className="pseudoStorage">{username}</span></p>
      </div>
      <div className="logoRibote">
        <FontAwesomeIcon className="home-cocktail" icon={faCocktail} />
        Ribote
        <FontAwesomeIcon className="home-cocktail" icon={faCocktail} />
      </div>
    </div>
  );
}

export default Banner;
