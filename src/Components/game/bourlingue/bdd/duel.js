export default [
    {
        id: 0,
        text: "{player1} et {player2} se défient au clash des mamans, celui qui fait le moins rire les autres",
        swallow : "boit {nbSwallow}",
        sex : "all",
        isRule: false
    },
    {
        id: 1,
        text: "{player1} et {player2} se défient. Imitez l'animal de votre choix. Le public vote pour la meilleur imitation.",
        swallow : "Le gagnant distribuera {nbSwallow}.",
        sex : "all",
        isRule: false
    }
];
