import * as types from '../../../utils/context/reducer/types';
import * as tools from './tools';
import GameCore from "./core";

export function getSave(dispatch) {
    let saveFromStorage = sessionStorage.getItem('game'); 
    saveFromStorage = JSON.parse(saveFromStorage);
    if (saveFromStorage?.core?.id) {
        if (saveFromStorage.round === (saveFromStorage.game?.length)) {
            dispatch({ type: types.REMOVE_CURRENT_GAME, data: {} });
            return false
        }
        dispatch({ type: types.SET_CURRENT_GAME, data: { ...saveFromStorage } });
        return saveFromStorage;
    }
    return false;
}

export function initGame(dispatch, players, parameters, gameCore, game, setGameCore, setGame, setRound, setNbSwallow, setCurrentText) {
    if (!gameCore) {
        const gc = new GameCore(players, parameters);
        setGameCore(gc);
    }
    if (gameCore && !game) {
        const save = getSave(dispatch);
        if (save) {
            gameCore.setSave(save.core)
            setGame(save.game);
            setRound(save.round)
            setNbSwallow(save.nbSwallow)
            setCurrentText(save.currentText)
            setNbSwallow()
        } else {
            const g = gameCore.init();
            setGame(g);
            setRound(0);
        }
    }
}

export function saveGame(dispatch, gameCore, game, round, nbSwallow, currentText) {
    if (gameCore && game) {
        dispatch({
            type: types.SET_CURRENT_GAME,
            data: { core: gameCore, game, round, nbSwallow, currentText }
        });
    }
}

export function setRules(newRule, game, round, currentRules, setCurrentRules) {
    const rules = currentRules;
    rules.push({
        text: newRule,
        startRound: round,
        endRound: tools.randomRound(round, (game.length - 1))
    })
    setCurrentRules(rules)
}

export function getText(gameCore, game, round, nbSwallow, currentRules, setCurrentRules, setNbSwallow, setCurrentText) {
    if (gameCore && game) {
        const g = game[round];
        if (g) {
            const {text, swallow} = tools.formatText(g, gameCore.getPlayers(), round, (game.length - 1), nbSwallow)
            if (g.isRule) {
                setRules(text, game, round, currentRules, setCurrentRules)
            }
            for (let i = 0; i < currentRules.length; i++) {
                if (round > (currentRules[i].startRound + currentRules[i].endRound)) {
                    currentRules.splice(i, 1);
                }
            }
            setCurrentRules(currentRules)
            setNbSwallow(swallow)
            setCurrentText(text)
        }
    }
}