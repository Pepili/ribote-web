import { v4 as uniqId } from 'uuid';

export const authenticate = (state, { data }) => {
  return { ...state, username: data.username, id: data.id, isAuthenticated: true };
};

export const register = (state, { data }) => {
  const id = uniqId();
  const user = { ...state, username: data, isAuthenticated: true, id };
  sessionStorage.setItem('user', JSON.stringify(user));
  return user;
};

export const logout = (state, { data }) => {
  sessionStorage.removeItem('user');
  return { ...state, username: '', isAuthenticated: false, id: null };
};

export const set_current_game = (state, { data }) => {
  sessionStorage.setItem('game', JSON.stringify(data));
  return { ...state, ...data };
};

export const remove_current_game = (state, { data }) => {
  sessionStorage.removeItem('game');
  return { ...state, currentGame: null };
};
