import {typeRateAppearance, typeID} from './constant';
import * as tools from './tools';
import getData from '../bdd';
import { v4 as uniqId } from 'uuid';

export default class GameCore {

    constructor(players, parameters) {
        this.id = uniqId();
        this.parameters = { ...tools.formatParameters(parameters)};
        this.players = players;
        this.appearances = typeRateAppearance;
        this.typeAppearance = [];
        this.game = [];
        this.historique = {};
    }

    getParameters() {
        return this.parameters;
    }

    getPlayers() {
        return this.players;
    }

    init() {
        this.typeAppearance = this.generateArrayOfAppearance();
        for (let i = 0; i < this.parameters.duration; i++) {
            const gameTypeID = this.typeAppearance[Math.floor(Math.random() * (this.typeAppearance.length))];
            const data = getData(typeID[gameTypeID]);
            if (data) {
                this.game.push(this.selectRandomText(data, gameTypeID));
            }
        }
        return this.game;
    }

    setSave(save) {
        this.id = save.id;
        this.parameters = save.parameters;
        this.players = save.players;
        this.appearances = save.appearances;
        this.typeAppearance = save.typeAppearance;
        this.game = save.game;
        this.historique = save.historique;
    }

    generateArrayOfAppearance(typeAppearance = []) {
        for (let i = 0; i < this.appearances.length; i++) {
            const appearance = this.appearances[i];
            const key = Object.keys(appearance)[0];
            if (appearance[key] > 0) {
                typeAppearance.push(Number(key));
                appearance[key]--;
                this.appearances[i] = appearance;
            }
        }
        let findZero = this.appearances.find(appearance => appearance[Object.keys(appearance)[0]] > 0);
        if (findZero) {
            return this.generateArrayOfAppearance(typeAppearance);
        } else {
            return typeAppearance;
        }
    }

    selectRandomText(data, gameType) {
        if (!this.historique) {
            this.historique = {}
        }
        if (!this.historique[gameType]){
            this.historique[gameType] = [];
        }
        let text = data[Math.floor(Math.random() * (data.length))];
        if (!this.isFirstAppearances(text.id, gameType)) {
            text = data[Math.floor(Math.random() * (data.length))];
        } 
        text.gameType = gameType
        this.historique[gameType].push(text);
        return text;
    }

    isFirstAppearances(id, gameType){
        if (this.historique[gameType]) {
            const findHistory = this.historique[gameType].find((his) => his.id === id);
            if (findHistory) {
                return false;
            }
        }
        return true;  
    }
}
